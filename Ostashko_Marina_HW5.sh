#!/bin/bash

# 1. Створіть користувача з іменем "bob".

sudo adduser bob


# 2. Додайте створеного користувача до групи sudo (щоб він міг виконувати команди як адміністратор).

sudo usermod -aG sudo bob
groups bob
# bob : bob sudo


# 3. Створіть сценарій у каталозі /home/bob/, який під час виконання змінить ім'я хоста для "ubuntu22". 
#    Атрибути виконання сценарію повинні бути встановлені виключно для користувача "bob".

change_hostname() {
	if [[ "${USER}" == "bob" ]]; then
		hostnamectl set-hostname bob-hostname
		echo "Well done!"
	else
		echo "You have no permission"
	fi
}

change_hostname


# 4. Запустіть сценарій. Перезавантажте систему. Увійти в систему як "bob" користувача.

#5. Встановіть "nginx". Перевірте, чи працює nginx, а також використовуйте netstat, щоб побачити, які порти є ВІДЧИНЕНО.

sudo apt install nginx
systemctl status nginx
netstat -l




